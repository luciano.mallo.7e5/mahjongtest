package com.example.twoequals

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.twoequals.databinding.ActivityScoreScreenBinding


class ScoreScreen : AppCompatActivity() {

    private lateinit var binding: ActivityScoreScreenBinding
    private var score: String = " "
    private var difficulty = " "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScoreScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.button2.setOnClickListener { backToMenu() }
        binding.button.setOnClickListener { playAgain() }

        val extras = intent.extras
        score = extras?.getInt("score").toString()
        difficulty = extras?.getString("difficulty").toString()

        val scoreView: TextView = findViewById<TextView>(binding.scoreView.id)
        scoreView.text = "$score Points"

    }

    private fun backToMenu() {
        val menu = Intent(this, MainActivity::class.java).apply {}
        startActivity(menu)
    }

    private fun playAgain() {
        val playAgain = Intent(this, Playing::class.java).apply {}
        playAgain.putExtra("difficulty", difficulty)
        startActivity(playAgain)

    }

}