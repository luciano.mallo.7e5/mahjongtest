package com.example.twoequals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.twoequals.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val difficulties = resources.getStringArray(R.array.difficulty)// access the spinner
        var difficulty = " "

        if (binding.Dificulties != null) {
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, difficulties)
            binding.Dificulties!!.adapter = adapter
            binding.Dificulties!!.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (difficulties[position] == "Difficulty") {
                        difficulty = difficulties[1]

                    } else {
                        difficulty = difficulties[position]
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    difficulty = difficulties[1]
                }
            }
        }
        // Binding to the playing activity
        binding.Start?.setOnClickListener { starGame(difficulty) }
        // Help Dialog Implemented
        binding.Help?.setOnClickListener {
            val builder = AlertDialog.Builder(this, R.style.AlertDialogStyle)
            builder.setNegativeButton("Close") { _, _ -> }
            builder
            builder.setTitle("Help")
            builder.setMessage(
                "The game starts with all the cards face down and player turns over two cards. " +
                        "If the two cards have the same picture, then they keep the cards, " +
                        "otherwise they turn the cards face down again." +
                        "\n\nHardcore Mode\n" +
                        "The game starts with nine cards face down and player turns over three cards. " +
                        "If the three cards have the same picture, then they keep the cards, " +
                        "otherwise they turn the cards face down again."
            )
            builder.show()
        }
    }

    fun starGame(dificulty: String) {

        //If no difficulty is selected the start button carry the user to the next Activity with the easy difficulty by default.

        val start = Intent(this, Playing::class.java).apply {}
        start.putExtra("difficulty", dificulty)
        startActivity(start)


    }
}

