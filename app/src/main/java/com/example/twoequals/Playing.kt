package com.example.twoequals

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.widget.Chronometer
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import com.example.twoequals.databinding.ActivityPlayingBinding
import kotlin.math.round


class Playing : AppCompatActivity() {
    private var timerStarted = false
    private var lastPause: Long = 0
    private lateinit var binding: ActivityPlayingBinding
    private var score = 0
    private var dificulty = " "
    private lateinit var buttons: Array<ImageButton>
    private var images: MutableList<Int> = mutableListOf()
    private var buttonState = mutableListOf<Int>()
    private val cardBack = R.drawable.fondocartas
    private var totalCards: Int = 0
    private var clicked: Int = 0
    private var turnOver = false
    private var lastClicked: Int = 0
    private var lastSecondClicked: Int = 0
    private lateinit var chronometer: Chronometer
    private var tries: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        chronometer = binding.Chronometer

        val triesView: TextView = findViewById(binding.Tries.id)
        triesView.text = "Tries: $tries"

        binding.PauseButton.setOnClickListener {
            pauseButtonHandler()
        }
        buttons = arrayOf(
            binding.Card1,
            binding.Card2,
            binding.Card3,
            binding.Card4,
            binding.Card5,
            binding.Card6,
            binding.Card7,
            binding.Card8,
            binding.Card9
        )
        checkForSavedInstanceState(savedInstanceState, triesView)


        if (dificulty == "Hardcore") {
            hardCoreBoard(triesView)
        } else {
            buildBoard(triesView)
        }
    }

    private fun checkForSavedInstanceState(savedInstanceState: Bundle?, triesView: TextView) {
        if (savedInstanceState != null) {
            images = savedInstanceState.getIntegerArrayList("images")!!
            dificulty = savedInstanceState.getString("Dificulty")!!
            tries = savedInstanceState.getInt("Tries")
            buttonState = savedInstanceState.getIntegerArrayList("buttonState")!!
            for (i in buttonState.indices) {
                //buttons[buttonState[i]] = images[buttonState]
                buttons[buttonState[i]].setBackgroundResource(images[buttonState[i]])
                buttons[buttonState[i]].tag = "turned"
            }
            totalCards = savedInstanceState.getInt("TotalCards")
            triesView.text = "Tries: $tries"

            chronometer.base = savedInstanceState.getLong("Chronometer")
            chronometer.start()
        } else {
            val extras = intent.extras
            dificulty = extras?.getString("difficulty").toString()
            fillVariables()
            images.shuffle()

        }
    }

    private fun pauseButtonHandler() {
        lastPause = SystemClock.elapsedRealtime()
        chronometer.stop()
        val builder = AlertDialog.Builder(this, R.style.AlertDialogStyle)
        builder.setTitle("GAME PAUSED")
        builder.setPositiveButton("BACK TO GAME") { _, _ ->
            chronometer.base = chronometer.base + SystemClock.elapsedRealtime() - lastPause
            chronometer.start()
        }
        builder.setNegativeButton("BACK TO MENU") { _, _ ->
            backToMenu()
        }
        builder.show()
    }

    private fun fillVariables() {
        //Swicth con nivel de dificultad y rellenar la variable segun corresponda.
        when (dificulty) {
            "Easy" -> {
                binding.Card7.isClickable = false
                binding.Card8.isClickable = false
                binding.Card9.isClickable = false
                binding.Card7.isGone = true
                binding.Card8.isGone = true
                binding.Card9.isGone = true
                images = mutableListOf(
                    R.drawable.pieza_1,
                    R.drawable.pieza_2,
                    R.drawable.pieza_3,
                    R.drawable.pieza_1,
                    R.drawable.pieza_2,
                    R.drawable.pieza_3
                )
            }
            "Hard" -> {

                binding.Card9.isClickable = false
                binding.Card9.isGone = true

                images = mutableListOf(
                    R.drawable.pieza_1,
                    R.drawable.pieza_2,
                    R.drawable.pieza_3,
                    R.drawable.pieza_4,
                    R.drawable.pieza_1,
                    R.drawable.pieza_2,
                    R.drawable.pieza_3,
                    R.drawable.pieza_4
                )
            }
            "Hardcore" -> {
                images = mutableListOf(
                    R.drawable.pieza_1,
                    R.drawable.pieza_1,
                    R.drawable.pieza_1,
                    R.drawable.pieza_2,
                    R.drawable.pieza_2,
                    R.drawable.pieza_2,
                    R.drawable.pieza_3,
                    R.drawable.pieza_3,
                    R.drawable.pieza_3
                )

            }
        }


    }

    private fun buildBoard(triesView: TextView) {
        for (i in 0 until images.size) {
            if (buttons[i].tag != "turned") {
                buttons[i].setBackgroundResource(cardBack)
                buttons[i].tag = "cardBack"
            }
            buttons[i].setOnClickListener {

                if (!timerStarted) {
                    chronometer.base = SystemClock.elapsedRealtime()
                    chronometer.start()
                    timerStarted = true
                }
                if (buttons[i].tag == "cardBack" && !turnOver) {
                    buttons[i].setBackgroundResource(images[i])
                    buttons[i].tag = ""
                    if (clicked == 0) {
                        lastClicked = i
                    }
                    clicked++
                } else if (buttons[i].tag != "cardBack") {
                    buttons[i].setBackgroundResource(cardBack)
                    buttons[i].tag = "cardBack"
                    clicked--
                }

                if (clicked == 2) {
                    turnOver = true
                    if (images[i] == images[lastClicked]) {
                        freezeEqualsCards(i)

                    }
                    if (images[i] != images[lastClicked]) {

                        Handler(Looper.getMainLooper()).postDelayed({

                            turnBackCards(i)

                        }, 600)

                    } else if (clicked == 0) {

                        turnOver = false

                    }
                }
                if (totalCards == (images.size / 2)) {
                    chronometer.stop()
                    goToscoreScreen()
                }


                triesView.text = "Tries: $tries"
            }

        }

    }

    private fun hardCoreBoard(triesView: TextView) {
        for (i in 0 until images.size) {
            if (buttons[i].tag != "turned") {
                buttons[i].setBackgroundResource(cardBack)
                buttons[i].tag = "cardBack"
            }
            buttons[i].setOnClickListener {

                if (!timerStarted) {
                    chronometer.base = SystemClock.elapsedRealtime()
                    chronometer.start()
                    timerStarted = true
                }
                if (buttons[i].tag == "cardBack" && !turnOver) {
                    buttons[i].setBackgroundResource(images[i])
                    buttons[i].tag = ""

                    if (clicked == 0) {
                        lastClicked = i
                    }
                    if (clicked == 1) {
                        lastSecondClicked = i
                    }
                    clicked++
                }

                if (clicked == 3) {
                    turnOver = true
                    if (images[i] == images[lastClicked] && images[lastClicked] == images[lastSecondClicked]) {
                        freezeEqualsCards(i)
                    }
                    if (images[i] != images[lastClicked] || images[lastClicked] != images[lastSecondClicked]) {

                        Handler(Looper.getMainLooper()).postDelayed({
                            turnBackCards(i)
                        }, 600)

                    } else if (clicked == 0) {
                        turnOver = false
                    }
                }
                if (totalCards == (images.size / 3)) {
                    chronometer.stop()
                    goToscoreScreen()
                }


                triesView.text = "Tries: $tries"
            }

        }

    }

    private fun turnBackCards(i: Int) {

        buttons[lastSecondClicked].setBackgroundResource(cardBack)
        buttons[lastSecondClicked].tag = "cardBack"
        buttons[lastClicked].setBackgroundResource(cardBack)
        buttons[lastClicked].tag = "cardBack"
        buttons[i].setBackgroundResource(cardBack)
        buttons[i].tag = "cardBack"
        turnOver = false
        clicked = 0
        tries += 1
    }

    private fun freezeEqualsCards(i: Int) {
        when (dificulty) {

            "Hard",
            "Easy" -> {
                buttons[i].tag = "Turned"
                buttons[lastClicked].tag = "Turned"
                buttons[i].isClickable = false
                buttons[lastClicked].isClickable = false
                totalCards++
                turnOver = false
                clicked = 0
                tries += 1
            }
            "Hardcore" -> {
                buttons[i].tag = "Turned"
                buttons[lastClicked].tag = "Turned"
                buttons[lastSecondClicked].tag = "Turned"
                buttons[i].isClickable = false
                buttons[lastClicked].isClickable = false
                buttons[lastSecondClicked].isClickable = false
                totalCards++
                turnOver = false
                clicked = 0
                tries += 1
            }
        }
    }

    private fun goToscoreScreen() {
        val scoreIntent = Intent(this, ScoreScreen::class.java).apply {}
        if (dificulty == "Easy") {
            score = round((1 / (tries).toDouble()) * 50).toInt()
        }
        if (dificulty == "Hard") {
            score = round((1 / (tries).toDouble()) * 100).toInt()
        }
        if (dificulty == "Hardcore") {
            score = round((1 / (tries).toDouble()) * 200).toInt()
        }

        scoreIntent.putExtra("score", score)
        scoreIntent.putExtra("difficulty", dificulty)
        startActivity(scoreIntent)
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putIntegerArrayList("images", ArrayList(images))
        for (i in buttons.indices) {
            if (buttons[i].tag == "Turned") {
                buttonState.add(i)
            }
        }
        savedInstanceState.putLong("Chronometer", (chronometer.base))
        savedInstanceState.putIntegerArrayList("buttonState", ArrayList(buttonState))
        savedInstanceState.putInt("Tries", tries)
        savedInstanceState.putString("Dificulty", dificulty)
        savedInstanceState.putInt("TotalCards", totalCards)


    }

    private fun backToMenu() {
        val menu = Intent(this, MainActivity::class.java).apply {}
        startActivity(menu)
    }
}
